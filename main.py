from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def algo():
    return 'homepage3'

@app.route('/modulos/<nombreModulo>')
def modulo(nombreModulo):
    return 'modulo {}'.format(nombreModulo) 

@app.route('/modulos/<nombreModulo>')
def profile(name):
    render_template('profile.html', name=name)

if __name__ == "__main__":
    app.run(debug=True)