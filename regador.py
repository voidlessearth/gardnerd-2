from datetime import datetime, timedelta
import threading, time
from timeloop import Timeloop
from luces import *

# SECTION Timer

tl = Timeloop()
@tl.job(interval=timedelta(minutes=5))
def toggleLuz():
    global estadoLuz
    horaActual = datetime.now()

    delta = timedelta(
        hours=horaActual.hour,
        minutes=horaActual.minute,
        seconds=horaActual.second,
        microseconds=horaActual.microsecond
    )

    tiempoRestantePrendidoLuz = horaPrendidoLuz - delta
    tiempoRestanteApagadoLuz = horaApagadoLuz - delta
    print(tiempoRestantePrendidoLuz.time(),tiempoRestanteApagadoLuz.time())

    if tiempoRestantePrendidoLuz.hour < 1 and tiempoRestantePrendidoLuz.minute < 1 and tiempoRestantePrendidoLuz.second < 1:
        estadoLuz = True
    
    if tiempoRestanteApagadoLuz.hour < 1 and tiempoRestanteApagadoLuz.minute < 1 and tiempoRestanteApagadoLuz.second < 1:
        estadoLuz = False
    print(estadoLuz)



if __name__ == "__main__":
    toggleLuz()
    tl.start(block=True)
 
# !SECTION

