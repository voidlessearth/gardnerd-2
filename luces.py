from datetime import datetime, timedelta

# ANCHOR Luz
estadoLuz = False

# ANCHOR Hora
horaActual = datetime.now()
horaPrendidoLuz = datetime.replace(horaActual,horaActual.year,horaActual.month,horaActual.day,20,0,0,0)
horaApagadoLuz = datetime.replace(horaActual,horaActual.year,horaActual.month,horaActual.day,16,0,0,0)
margenTiempoPrendidoLuces = 1


# print(horaPrendidoLuz - timedelta(hours = margenTiempoPrendidoLuces))

data = {
    'horaActual' : horaActual.time(),
    'horaPrendidoLuz' : horaPrendidoLuz,
    'horaApagadoLuz' : horaApagadoLuz
}

delta = timedelta(
    hours=horaActual.hour,
    minutes=horaActual.minute,
    seconds=horaActual.second,
    microseconds=horaActual.microsecond
)

tiempoRestantePrendidoLuz = horaPrendidoLuz - delta
segundosRestantesPrendidoLuz = tiempoRestantePrendidoLuz.hour * 360 + tiempoRestantePrendidoLuz.minute * 60 + tiempoRestantePrendidoLuz.second * 10 + tiempoRestantePrendidoLuz.microsecond / 10**6
# !SECTION
